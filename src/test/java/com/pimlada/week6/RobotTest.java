package com.pimlada.week6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RobotTest {
    @Test
    public void shouldCreateRobotSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(10, robot.getX());
        assertEquals(11, robot.getY());
    }

    @Test
    public void shouldCreateRobotSuccess2() {
        Robot robot = new Robot("Robot", 'R');
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(0, robot.getX());
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldCreateRobotUpSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up();
        assertEquals(true, result);
        assertEquals(10, robot.getY());
    }

    @Test
    public void shouldCreateRobotUpFailAtMin() {
        Robot robot = new Robot("Robot", 'R', 10, Robot.Y_MIN);
        boolean result = robot.up();
        assertEquals(false, result);
        assertEquals(Robot.Y_MIN, robot.getY());
    }

    @Test
    public void shouldCreateRobotUpNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(5);
        assertEquals(true, result);
        assertEquals(6, robot.getY());
    }

    @Test
    public void shouldCreateRobotUpNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(11);
        assertEquals(true, result);
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldCreateRobotUpNFail1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(12);
        assertEquals(false, result);
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldCreateRobotDownSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        boolean result = robot.down();
        assertEquals(true, result);
        assertEquals(10, robot.getY());
    }
    
    @Test
    public void shouldCreateRobotDownBeforeMaxSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, Robot.Y_MAX - 1);
        boolean result = robot.down();
        assertEquals(true, result);
        assertEquals(Robot.Y_MAX, robot.getY());
    }

    @Test
    public void shouldCreateRobotDownAtMax() {
        Robot robot = new Robot("Robot", 'R', 10, Robot.Y_MAX);
        boolean result = robot.down();
        assertEquals(false, result);
        assertEquals(Robot.Y_MAX, robot.getY());
    }

    @Test
    public void shouldCreateRobotDownNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        boolean result = robot.down(5);
        assertEquals(true, result);
        assertEquals(14, robot.getY());
    }

    @Test
    public void shouldCreateRobotDownNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        boolean result = robot.down(10);
        assertEquals(true, result);
        assertEquals(19, robot.getY());
    }

    @Test
    public void shouldCreateRobotDownNFail1() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        boolean result = robot.down(11);
        assertEquals(false, result);
        assertEquals(19, robot.getY());
    }

    @Test
    public void shouldCreateRobotLeftSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.left();
        assertEquals(true, result);
        assertEquals(9, robot.getX());
    }

    @Test
    public void shouldCreateRobotLeftFailAtMin() {
        Robot robot = new Robot("Robot", 'R', Robot.X_MIN, 10);
        boolean result = robot.left();
        assertEquals(false, result);
        assertEquals(Robot.X_MIN, robot.getX());
    }

    @Test
    public void shouldCreateRobotLeftNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.left(5);
        assertEquals(true, result);
        assertEquals(5, robot.getX());
    }

    @Test
    public void shouldCreateRobotLeftNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.left(10);
        assertEquals(true, result);
        assertEquals(0, robot.getX());
    }

    @Test
    public void shouldCreateRobotLeftNFail1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.left(11);
        assertEquals(false, result);
        assertEquals(0, robot.getX());
    }

    @Test
    public void shouldCreateRobotRightSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.right();
        assertEquals(true, result);
        assertEquals(11, robot.getX());
    }

    @Test
    public void shouldCreateRobotRightBeforeMaxSuccess() {
        Robot robot = new Robot("Robot", 'R', Robot.X_MAX - 1, 10);
        boolean result = robot.right();
        assertEquals(true, result);
        assertEquals(Robot.X_MAX, robot.getX());
    }

    @Test
    public void shouldCreateRobotRightAtMax() {
        Robot robot = new Robot("Robot", 'R', Robot.X_MAX, 10);
        boolean result = robot.right();
        assertEquals(false, result);
        assertEquals(Robot.X_MAX, robot.getX());
    }

    @Test
    public void shouldCreateRobotRightNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.right(5);
        assertEquals(true, result);
        assertEquals(15, robot.getX());
    }

    @Test
    public void shouldCreateRobotRightNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.right(9);
        assertEquals(true, result);
        assertEquals(19, robot.getX());
    }

    @Test
    public void shouldCreateRobotRightNFail1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.right(10);
        assertEquals(false, result);
        assertEquals(19, robot.getX());
    }
}
