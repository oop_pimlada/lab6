package com.pimlada.week6;

public class BookBankApp {
    public static void main(String[] args) {
        BookBank pimlada = new BookBank("Pimlada", 100.0);
        pimlada.print();
        pimlada.deposit(50);
        pimlada.print();
        pimlada.withdraw(50);
        pimlada.print();

        BookBank prayood = new BookBank("Prayoood", 1);
        prayood.deposit(1000000);
        prayood.withdraw(10000000);
        prayood.print();

        BookBank praweeet = new BookBank("Praweeet", 10);
        praweeet.deposit(10000000);
        praweeet.withdraw(1000000);
        praweeet.print();
    }
}
